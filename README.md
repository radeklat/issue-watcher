# issue-watcher
Sometimes it happens that you discover a bug in a library that you are using and have to create a workaround (technical debt). However, it would be good to also remove the workaround once a bugfix is released.

This library provides several useful assertions that are watching when an issue is closed and failing a test to let you know fixed functionality is available. A good way to automatically manage and reduce known technical debt.

**The project is [hosted on GitHub](https://github.com/radeklat/issue-watcher).**

This repository is used for testing issue tracker only.